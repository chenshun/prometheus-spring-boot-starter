package com.kuding.pojos.servicemonitor;

public enum ServiceStatus {

	UP, DOWN, RECOVERED;
}
